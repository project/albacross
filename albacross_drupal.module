<?php

/**
 * @file
 * A block module that adds Albacross script to footer area.
 */

/**
 * Implements hook_help().
 */
function albacross_drupal_help($path, $arg) {
  switch ($path) {
    case "admin/help#albacross_drupal":
      return t("Adds Albacross script to footer area");

  }
}

/**
 * Implements hook_menu().
 */
function albacross_drupal_menu() {
  $items = array();

  $items['admin/config/system/albacross_drupal'] = array(
    'title' => 'Albacross tracking',
    'description' => 'Configuration of Albacross integration',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('albacross_drupal_form'),
    'access arguments' => array('administer site configuration'),
    'type' => MENU_NORMAL_ITEM,
  );

  return $items;
}

/**
 * Page callback: Albacross module settings.
 *
 * @see albacross_drupal_menu()
 */
function albacross_drupal_form() {
  $form['albacross_drupal_trackid'] = array(
    '#type' => 'textfield',
    '#title' => t('Your Track ID which could be found in Albacross app'),
    '#default_value' => variable_get('albacross_drupal_trackid', ''),
    '#description' => t('Your Track ID'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}

/**
 * Implements template_preprocess_page().
 */
function albacross_drupal_preprocess_page(&$variables) {
  $trackid = variable_get('albacross_drupal_trackid', '');
  if (!empty($trackid)) {
    $js = "_nQc = '" . $trackid . "';
      _nQs = 'Drupal-Module';
      _nQsv = '1.0.0';
      _nQt = new Date().getTime();
      (function() {
        var no = document.createElement('script'); no.type = 'text/javascript'; no.async = true;
        no.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'serve.albacross.com/track.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(no, s);
    })();";
    drupal_add_js($js, array(
      'type' => 'inline',
      'scope' => 'footer',
      'weight' => 5,
    ));
  }
}

/**
 * Implements hook_form_validate().
 */
function albacross_drupal_form_validate($form, &$form_state) {
  $trackId = $form_state['values']['albacross_drupal_trackid'];
  if (!is_numeric($trackId)) {
    form_set_error('albacross_drupal_trackid', t('You must enter correct Track ID for your Albacross account'));
  }
  elseif ($trackId <= 0) {
    form_set_error('albacross_drupal_trackid', t('You should not use minus in front of your Track ID'));
  }
}
