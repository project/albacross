CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Requirements
 * Configuration

INTRODUCTION
------------

Albacross

This module simplifies the process of integrating the
Albacross tracking code with your Drupal website.

INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

Navigate to administer >> modules. Enable Albacross.

REQUIREMENTS
------------

This module doesn't require any additional module

CONFIGURATION
-------------

After installation finished and you've enabled module
you can head to yourwebsite.com/admin/config/system/albacross_drupal
to configure plugin setting up the Tracking ID which
you can find in your member area on app.albacross.com website
